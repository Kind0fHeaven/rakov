jQuery('.event-day.event-day__has-event').click(function() {
	var info = jQuery(this).find('.info-card:eq(0)').clone().css('left','300px');
	jQuery('.event-block__info').find('div').animate({'left':'-300px'}, 150)
	setTimeout(function() {
		jQuery('.event-block__info').find('div').remove();
		jQuery('.event-block__info').append($(info));
		jQuery('.event-block__info').find('.info-card').animate({'left':0},150)
	}, 200)
	jQuery('.event-day.active').removeClass('active')
	jQuery(this).addClass('active')
})