jQuery('.js__faq--item').click(function(){
	var	question = jQuery(this).parent()
	if (question.hasClass('contacts-faq__item_opened')) {
		question.removeClass('contacts-faq__item_opened')
	} else {
		jQuery('.contacts-faq__item_opened').removeClass('contacts-faq__item_opened')
		jQuery(question).addClass('contacts-faq__item_opened')
	}
})