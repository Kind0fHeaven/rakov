// Home slider
jQuery('.js__home-slider').on('init', function(event, slick) {
  jQuery(this).addClass('initiated')
})
jQuery('.js__home-slider').slick({
  fnCanGoNext: function(a,b) {return true},
  dots: true,
  infinite: true,
  speed: 500,
  fade: true,
  cssEase: 'linear'
});

// Video slider
jQuery('.js__education--video-slider').slick({
  fnCanGoNext: function(a,b) {return true},
  slidesToShow: 2,
  slidesToScroll: 2,
  appendArrows: $('.js__education--arrows'),
    prevArrow: '<a href="javascript:void(0)" class="slider-arrow_small"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    nextArrow: '<a href="javascript:void(0)" class="slider-arrow_small slider-arrow_small_left"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
})
jQuery('.album__images').slick({
  fnCanGoNext: function(a,b) {return true},
  responsive: [
      {
        breakpoint: 4000,
        settings: 'unslick'
      },
      {
        breakpoint: 1170,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
          arrows: false,
          dots: false,
          centerMode: true,
          centerPadding: '100px'
        }
      },
      {
        breakpoint: 980,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          arrows: false,
          dots: false,
          centerMode: true,
          centerPadding: '100px'
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          dots: false,
          centerMode: true,
          centerPadding: '100px'
        }
      },
      {
        breakpoint: 500,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          arrows: false,
          dots: false,
          centerMode: true,
          centerPadding: '20px'
        }
      }
    ]
})

// Reviews slider
jQuery('.js__reviews--slider').slick({
  fnCanGoNext: function(a,b) {return true},
  slidesToShow: 2,
  slidesToScroll: 2,
  dots: true,
  arrows: true,
  appendArrows: $('.js__reviews--arrows'),
    prevArrow: '<a href="javascript:void(0)" class="slider-arrow_small"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    nextArrow: '<a href="javascript:void(0)" class="slider-arrow_small slider-arrow_small_left"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    customPaging: function(slick, index) {
        return '<span class="dot"></span>';
    },
    responsive: [
      {
        breakpoint: 1170,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
})
jQuery('.js__reviews-wide--slider').slick({
  fnCanGoNext: function(a,b) {return true},
  slidesToShow: 1,
  slidesToScroll: 1,
  dots: true,
  arrows: true,
  appendArrows: $('.js__reviews-wide--arrows'),
    prevArrow: '<a href="javascript:void(0)" class="slider-arrow_small"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    nextArrow: '<a href="javascript:void(0)" class="slider-arrow_small slider-arrow_small_left"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    customPaging: function(slick, index) {
        return '<span class="dot"></span>';
    }
})

// Documents slider
jQuery('.js__documents--slider').slick({
  fnCanGoNext: function(a,b) {return true},
  slidesToShow: 4,
  slidesToScroll: 4,
  appendArrows: $('.js__documents--arrows'),
    prevArrow: '<a href="javascript:void(0)" class="slider-arrow_small"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    nextArrow: '<a href="javascript:void(0)" class="slider-arrow_small slider-arrow_small_left"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    responsive: [
      {
        breakpoint: 1170,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 550,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
})
// Documents slider
jQuery('.js__staff--slider').slick({
  fnCanGoNext: function(a,b) {return true},
  slidesToShow: 4,
  slidesToScroll: 4,
  appendArrows: $('.js__staff--arrows'),
    prevArrow: '<a href="javascript:void(0)" class="slider-arrow_small"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    nextArrow: '<a href="javascript:void(0)" class="slider-arrow_small slider-arrow_small_left"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    responsive: [
      {
        breakpoint: 1170,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: '30%'
        }
      },{
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: false
        }
      }
    ]
})
// Documents slider
jQuery('.js__news-widget--slider').slick({
  fnCanGoNext: function(a,b) {return true},
  slidesToShow: 1,
  slidesToScroll: 1,
  appendArrows: $('.js__news-widget--arrows'),
    prevArrow: '<a href="javascript:void(0)" class="slider-arrow_small"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    nextArrow: '<a href="javascript:void(0)" class="slider-arrow_small slider-arrow_small_left"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
     responsive: [
      {
        breakpoint: 1170,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToScroll:1,
          slidesToShow:1
        }
      }
    ]
})
// Profit-widget slider
jQuery('.js__profit-widget--slider').slick({
  fnCanGoNext: function(a,b) {return true},
  slidesToShow: 1,
  slidesToScroll: 1,
  appendArrows: $('.js__profit-widget--arrows'),
    prevArrow: '<a href="javascript:void(0)" class="slider-arrow_small"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    nextArrow: '<a href="javascript:void(0)" class="slider-arrow_small slider-arrow_small_left"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>'
})
// Interesting slider
jQuery('.js__interesting--slider').slick({
  fnCanGoNext: function(a,b) {return true},
  slidesToShow: 2,
  slidesToScroll: 2,
  appendArrows: $('.js__interesting--arrows'),
    prevArrow: '<a href="javascript:void(0)" class="slider-arrow_small"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    nextArrow: '<a href="javascript:void(0)" class="slider-arrow_small slider-arrow_small_left"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }   
    ]
})
// Interesting-wide slider
jQuery('.js__interesting-wide--slider').slick({
  fnCanGoNext: function(a,b) {return true},
  slidesToShow: 3,
  slidesToScroll: 3,
  appendArrows: $('.js__interesting-wide--arrows'),
    prevArrow: '<a href="javascript:void(0)" class="slider-arrow_small"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    nextArrow: '<a href="javascript:void(0)" class="slider-arrow_small slider-arrow_small_left"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    responsive: [
      {
        breakpoint: 1170,
        settings: {
          slidesToScroll: 2,
          slidesToShow: 2
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1
        }
      }
    ]
})
// Info widget slider
jQuery('.js__slider-info-widget--slider').slick({
  fnCanGoNext: function(a,b) {return true},
  slidesToShow: 1,
  slidesToScroll: 1,
  appendArrows: $('.js__slider-info-widget--arrows'),
    prevArrow: '<a href="javascript:void(0)" class="slider-arrow_small"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    nextArrow: '<a href="javascript:void(0)" class="slider-arrow_small slider-arrow_small_left"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    responsive: [
      {
        breakpoint: 1170,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },   
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }   
    ]
})
// Wide events slider
jQuery('.js__wide-events--slider').slick({
  fnCanGoNext: function(a,b) {return true},
  slidesToShow: 2,
  slidesToScroll: 2,
  appendArrows: $('.js__wide-events--arrows'),
    prevArrow: '<a href="javascript:void(0)" class="slider-arrow_small"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    nextArrow: '<a href="javascript:void(0)" class="slider-arrow_small slider-arrow_small_left"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1
        }
      }
    ]
})
// Reviews events slider
jQuery('.js__reviews-widget--slider').slick({
  fnCanGoNext: function(a,b) {return true},
  slidesToShow: 1,
  slidesToScroll: 1,
  appendArrows: $('.js__reviews-widget--arrows'),
    prevArrow: '<a href="javascript:void(0)" class="slider-arrow_small"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    nextArrow: '<a href="javascript:void(0)" class="slider-arrow_small slider-arrow_small_left"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>'
})
// Reviews events slider
jQuery('.js__branches-widget--slider').slick({
  fnCanGoNext: function(a,b) {return true},
  slidesToShow: 1,
  slidesToScroll: 1,
  appendArrows: $('.js__branches-widget--arrows'),
    prevArrow: '<a href="javascript:void(0)" class="slider-arrow_small"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    nextArrow: '<a href="javascript:void(0)" class="slider-arrow_small slider-arrow_small_left"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>'
})
// Video widget slider
jQuery('.js__video-widget--slider').slick({
  fnCanGoNext: function(a,b) {return true},
  slidesToShow: 1,
  slidesToScroll: 1,
  appendArrows: $('.js__video-widget--arrows'),
    prevArrow: '<a href="javascript:void(0)" class="slider-arrow_small"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    nextArrow: '<a href="javascript:void(0)" class="slider-arrow_small slider-arrow_small_left"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>'
})
// Photos widget slider
jQuery('.js__photos-widget--slider').slick({
  fnCanGoNext: function(a,b) {return true},
  slidesToShow: 1,
  slidesToScroll: 1,
  appendArrows: $('.js__photos-widget--arrows'),
    prevArrow: '<a href="javascript:void(0)" class="slider-arrow_small"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    nextArrow: '<a href="javascript:void(0)" class="slider-arrow_small slider-arrow_small_left"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>'
})
// Video widget slider
jQuery('.js__popular-tests--slider').slick({
  fnCanGoNext: function(a,b) {return true},
  slidesToShow: 1,
  slidesToScroll: 1,
  appendArrows: $('.js__popular-tests--arrows'),
    prevArrow: '<a href="javascript:void(0)" class="slider-arrow_small"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    nextArrow: '<a href="javascript:void(0)" class="slider-arrow_small slider-arrow_small_left"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>'
})
// Events1 widget slider
jQuery('.js__event-block--slider').slick({
  fnCanGoNext: function(a,b) {return true},
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  dots: false,
  asNavFor: '.js__event-block--slider--navigation'
})
jQuery('.js__event-block--slider--navigation').slick({
  fnCanGoNext: function(a,b) {return true},
  slidesToShow: 1,
  slidesToScroll: 1,
  asNavFor: '.js__event-block--slider',
  arrows: true,
  appendArrows: $('.js__event-block--slider--navigation'),
  prevArrow: '<a href="javascript:void(0)" class="slider-arrow_small"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
  nextArrow: '<a href="javascript:void(0)" class="slider-arrow_small slider-arrow_small_left"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
  centerMode: true,
  focusOnSelect: true,
  centerPadding: 0
})
