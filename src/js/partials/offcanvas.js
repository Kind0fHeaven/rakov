jQuery('.js__menu-button').click(function() {
	var modal = jQuery('.offcanvas');
	var left = jQuery(window).outerWidth();
	jQuery(modal).css({left: -left}).show();
	jQuery(modal).animate({left: 0}, 500);	
	jQuery('body').toggleClass('offcanvas_opened')
})

jQuery('.js__offcanvas--cross').click(function() {
	var modal = jQuery(this).closest('.offcanvas');
	jQuery(modal).animate({left: modal.outerWidth()*(-1)}, 500);
	setTimeout(function() {
		jQuery(modal).hide().css({left: ''});
	}, 500)
	jQuery('body').toggleClass('offcanvas_opened')
})

var header = jQuery('.header:eq(0)'),
	scrollPrevious = 0;

jQuery(window).on('scroll', function() {
	var scrolled = jQuery(window).scrollTop();
	if (scrolled < jQuery(header).outerHeight() || scrollPrevious <= scrolled) {
		jQuery(header).removeClass('scrolling');
	} else {
		jQuery(header).addClass('scrolling');
	}
	scrollPrevious = scrolled;
})
