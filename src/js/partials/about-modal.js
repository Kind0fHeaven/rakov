jQuery('.js__about-me-open').click(function() {
	var modal = jQuery('.about-modal.js__modal');
	var width = jQuery(modal).outerWidth();
	var left = (jQuery(window).outerWidth() - width)/2;
	jQuery(modal).attr('data-left', left)
	jQuery('.opacity').fadeIn(300);
	setTimeout(function() {
		jQuery(modal).css({left: jQuery(window).outerWidth() + width}).show()
		jQuery(modal).animate({left: left}, 500);	
	}, 100)
})
jQuery('.about-modal--block').scrollbar();

