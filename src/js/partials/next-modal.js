jQuery('.js__next-open').click(function() {
	var modal = jQuery('.next-modal.js__modal');
	var width = jQuery(modal).outerWidth();
	var left = (jQuery(window).outerWidth() - width)/2;
	jQuery(modal).attr('data-left', left)
	jQuery('.opacity').fadeIn(300);
	setTimeout(function() {
		jQuery(modal).css({left: jQuery(window).outerWidth() + width}).show()
		jQuery(modal).animate({left: left}, 500);	
	}, 100)
})
jQuery('.next-modal .send-button').click(function(event) {
	if (jQuery('#nextPhone').val() == '' || jQuery('#nextEmail').val() == '') {
		if (jQuery('#nextPhone').val() == '') {
			jQuery('#nextPhone').parent().addClass('wrong')
		}
		if (jQuery('#nextEmail').val() == '') {
			jQuery('#nextEmail').parent().addClass('wrong')
		}
		event.preventDefault()
		return 0;
	}
})
function modalDone() {
	try {
		$('.js__modal:visible .js__modal-cross').trigger('click');
		$(window).scrollTop(0)
		var classList = $('.js__modal:visible').attr('class').split(/\s+/);
		var modal;
		$.each(classList, function(index, item) {
		    if (item !== 'js__modal') {
		    	var class = '.' + item + '--done';
		    	console.log(class)
		    	modal = jQuery(class);
		    	console.log(modal)
		    }
		});
		var width = jQuery(modal).outerWidth();
		var left = (jQuery(window).outerWidth() - width)/2;
		setTimeout(function() {
			jQuery(modal).attr('data-left', left)
			jQuery('.opacity').fadeIn(300);
			setTimeout(function() {
				jQuery(modal).css({left: jQuery(window).outerWidth() + width}).show()
				jQuery(modal).animate({left: left}, 500);	
			}, 100)
		}, 600)
	} catch (err) {
		console.log(err)
	}
}
jQuery('.next-modal--ok').click(function() {
	var modal = jQuery(this).closest('.js__modal');
	jQuery(modal).animate({left: modal.outerWidth()*(-1)}, 500);
	setTimeout(function() {
		jQuery('.opacity').fadeOut(300);
		jQuery(modal).hide().css({left: ''});
	}, 500)
})