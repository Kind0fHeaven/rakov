jQuery('.js__education--video-slider').slick({
	slidesToShow: 2,
	slidesToScroll: 2,
	appendArrows: $('.js__education--arrows'),
    prevArrow: '<a href="javascript:void(0)" class="slider-arrow_small"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>',
    nextArrow: '<a href="javascript:void(0)" class="slider-arrow_small slider-arrow_small_left"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--047-next"></svg></a>'
})