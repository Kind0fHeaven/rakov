jQuery('.js__notify-open').click(function() {
	var modal = jQuery('.notification-modal.js__modal');
	var width = jQuery(modal).outerWidth();
	var left = (jQuery(window).outerWidth() - width)/2;
	jQuery(modal).attr('data-left', left)
	jQuery('.opacity').fadeIn(300);
	setTimeout(function() {
		jQuery(modal).css({left: jQuery(window).outerWidth() + width}).show()
		jQuery(modal).animate({left: left}, 500);	
		jQuery(window).trigger('resize');
	}, 100)
})
jQuery('.notification-modal--slider').slick({
  	dots: false,
  	infinite: false,
  	arrows: true,
  	speed: 300,
  	slidesToShow: 1,
  	adaptiveHeight: true,
  	slidesToScroll: 1,
  	appendArrows: $('.notification-modal--buttons'),
   	prevArrow: '<a href="javascript:void(0)" class="notification-modal--prev"><svg class="icon"><use xlink:href="./img/sprite.svg#icons--007-right-arrow"></svg>Назад</a>',
    nextArrow: '<a href="javascript:void(0)" class="notification-modal--next">Продолжить <svg class="icon"><use xlink:href="./img/sprite.svg#icons--007-right-arrow"></svg></a>',
    swipe: false,
    fnCanGoNext: function(instance, currentSlide) {
    	var currentSlide = instance.$slides.eq(currentSlide);
        var checkbox = currentSlide.find("input[type=checkbox]:checked").length;

        if (!checkbox) {
        	return false;
        } else {
        	return true;
        }
    } 
}).on('beforeChange', function(event, slick, currentSlide, nextSlide) {
	button = jQuery(this).parent().find('.send-button');
	if (nextSlide == (slick.slideCount - 1)) {
		button.addClass('is-shown')
	} else {
		button.removeClass('is-shown')
	}
	jQuery('.notification-modal--step.current--step').removeClass('current--step')
	jQuery('.notification-modal--step[data-step="' + (nextSlide + 1) + '"]').addClass('current--step')
	jQuery('.notification-modal--step').each(function() {
		if (parseInt(jQuery(this).attr('data-step')) < (nextSlide + 1)) {
			jQuery(this).addClass('done--step')
		} else {
			jQuery(this).removeClass('done--step')
		}
	})
}).on('afterChange', function() {
	if (jQuery('.slick-current input[type="checkbox"]:checked').length) {
		jQuery('.notification-modal--next').removeClass('disabled')
	} else {
		jQuery('.notification-modal--next').addClass('disabled')
	}
})
jQuery('.notification-modal .send-button').click(function(event) {
	if (jQuery('#notifyPhone').val() == '' || jQuery('#notifyEmail').val() == '') {
		if (jQuery('#notifyPhone').val() == '') {
			jQuery('#notifyPhone').parent().addClass('wrong')
		}
		if (jQuery('#notifyEmail').val() == '') {
			jQuery('#notifyEmail').parent().addClass('wrong')
		}
		event.preventDefault()
		return 0;
	}
})
var params = window
    .location
    .search
    .replace('?','')
    .split('&')
    .reduce(
        function(p,e){
            var a = e.split('=');
            p[ decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
            return p;
        },
        {}
    );
if (params['sended'] === 'notify' && jQuery('.notification-modal--done').length) {
	$(window).scrollTop(0)
	var modal = jQuery('.notification-modal--done');
	var width = jQuery(modal).outerWidth();
	var left = (jQuery(window).outerWidth() - width)/2;
	jQuery(modal).attr('data-left', left)
	jQuery('.opacity').fadeIn(300);
	setTimeout(function() {
		jQuery(modal).css({left: jQuery(window).outerWidth() + width}).show()
		jQuery(modal).animate({left: left}, 500);	
	}, 100)
}
jQuery('.notification-modal--ok').click(function() {
	var modal = jQuery(this).closest('.js__modal');
	jQuery(modal).animate({left: modal.outerWidth()*(-1)}, 500);
	setTimeout(function() {
		jQuery('.opacity').fadeOut(300);
		jQuery(modal).hide().css({left: ''});
	}, 500)
})
jQuery('.notification-modal--next').addClass('disabled')
jQuery('.notification-modal--slider input[type="checkbox"]').on('change', function() {
	console.log(jQuery('.slick-current input[type="checkbox"]:checked').length)
	if (jQuery('.slick-current input[type="checkbox"]:checked').length) {
		jQuery('.notification-modal--next').removeClass('disabled')
	} else {
		jQuery('.notification-modal--next').addClass('disabled')
	}
})