jQuery('.js__sended-open').click(function() {
	var modal = jQuery('.sended-modal--done.js__modal');
	var width = jQuery(modal).outerWidth();
	var left = (jQuery(window).outerWidth() - width)/2;
	jQuery(modal).attr('data-left', left)
	jQuery('.opacity').fadeIn(300);
	setTimeout(function() {
		jQuery(modal).css({left: jQuery(window).outerWidth() + width}).show()
		jQuery(modal).animate({left: left}, 500);	
	}, 100)
})
jQuery('.sended-modal--ok').click(function() {
	var modal = jQuery(this).closest('.js__modal');
	jQuery(modal).animate({left: modal.outerWidth()*(-1)}, 500);
	setTimeout(function() {
		jQuery('.opacity').fadeOut(300);
		jQuery(modal).hide().css({left: ''});
	}, 500)
})