// jQuery(new Image()).attr('src', jQuery('.home-slider__slide:eq(0) > .home-slider__slide--img-block > img').attr('src')).load(function() {
//     jQuery('.home-slider__wrapper').addClass('done')
// // });
// jQuery(window).load(function() {
// 	jQuery('.home-slider__wrapper').addClass('done')
// }) 
setTimeout(function() {
	jQuery('.home-slider__wrapper').addClass('done')
}, 1500)
if (jQuery('.author-widget').length) {
	jQuery('.main-content').parent().addClass("has-author")
	if (jQuery('.small-banner').length) {
		jQuery('.main-content').parent().addClass('has-small-banner')
	}
}
if (jQuery('.timer-widget').length) {
	jQuery('.main-content').parent().addClass("has-author").addClass('has-timer')
	if (jQuery('.small-banner').length) {
		jQuery('.main-content').parent().addClass('has-small-banner')
	}
}
function handler(event) {
	event.preventDefault()
}
jQuery(document).ready(function() {

	// Modals close
		jQuery('.js__modal-cross').click(function() {
			var modal = jQuery(this).closest('.js__modal');
			jQuery(modal).animate({left: modal.outerWidth()*(-1)}, 500);
			setTimeout(function() {
				jQuery('.opacity').fadeOut(300);
				jQuery(modal).hide().css({left: ''});
			}, 500)
			if (jQuery('body').hasClass('offcanvas_opened')) {
				jQuery('body').removeClass('offcanvas_opened')
				window.removeEventListener('touchmove', handler);
			}
		})
		jQuery('.dropdown-button').click(function() {
			var block = jQuery(this).parent().children('.comment--text'),
				svg = jQuery(this).children('svg.icon').eq(0);
			if (jQuery(this).hasClass('opened')) {				
				jQuery(this).text('Показать весь');
			} else {
				jQuery(this).text('Свернуть');
			}
			jQuery(this).append(svg)
			jQuery(this).toggleClass('opened')
			block.toggleClass('opened')
		})
		jQuery('.hovered').click(function() {
			jQuery(this).toggleClass('hovered_true')
		})

	// Adaptive modal

	jQuery('.header__block--search-block').click(function() {
		if (jQuery(window).outerWidth() > 1170) {
			return 0
		}
		var modal = jQuery('.search-adaptive.js__modal');
		var width = jQuery(modal).outerWidth();
		var left = (jQuery(window).outerWidth() - width)/2;
		jQuery(modal).attr('data-left', left)
		setTimeout(function() {
			jQuery(modal).css({left: jQuery(window).outerWidth() + width}).show()
			jQuery(modal).animate({left: left}, 500);	
		}, 100)
		jQuery('body').addClass('offcanvas_opened')
		window.addEventListener('touchmove', handler,{ passive: false });
	})

	jQuery('.header__block--phone').click(function(event) {
		if (jQuery(window).outerWidth() < 768) {
			event.preventDefault();
			jQuery(this).toggleClass('_active')
			jQuery('#headerMobilePhone').toggleClass('_active')
		}
	})

	//= ui.js
	//= search.js
	//= offcanvas.js
	//= sliders.js
	//= faq.js
	//= tabs.js
	//= notification.js
	//= next-modal.js
	//= about-modal.js
	//= event.js
	//= sended.js
	//= anketa-modal.js

})