jQuery('.js__anketa-open').click(function() {
	var modal = jQuery('.anketa-modal.js__modal');
	var width = jQuery(modal).outerWidth();
	var left = (jQuery(window).outerWidth() - width)/2;
	jQuery(modal).attr('data-left', left)
	jQuery('.opacity').fadeIn(300);
	setTimeout(function() {
		jQuery(modal).css({left: jQuery(window).outerWidth() + width}).show()
		jQuery(modal).animate({left: left}, 500);	
		jQuery(window).trigger('resize');
	}, 100)
})
jQuery('.anketa--list input[type="radio"]').change(function() {
	jQuery('.anketa-modal--next').removeClass('disabled').prop('disabled', false)
})


var params = window
    .location
    .search
    .replace('?','')
    .split('&')
    .reduce(
        function(p,e){
            var a = e.split('=');
            p[ decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
            return p;
        },
        {}
    );
if (params['anketa'] === 'sended' && jQuery('.anketa-modal--done').length) {
	$(window).scrollTop(0)
	var modal = jQuery('.anketa-modal--done');
	var width = jQuery(modal).outerWidth();
	var left = (jQuery(window).outerWidth() - width)/2;
	jQuery(modal).attr('data-left', left)
	jQuery('.opacity').fadeIn(300);
	setTimeout(function() {
		jQuery(modal).css({left: jQuery(window).outerWidth() + width}).show()
		jQuery(modal).animate({left: left}, 500);	
	}, 100)
}

jQuery('.anketa-modal--ok').click(function() {
	var modal = jQuery(this).closest('.js__modal');
	jQuery(modal).animate({left: modal.outerWidth()*(-1)}, 500);
	setTimeout(function() {
		jQuery('.opacity').fadeOut(300);
		jQuery(modal).hide().css({left: ''});
	}, 500)
})