'use strict';
var availableTags = [
    "Санкт-Петербург",
	"Новосибирск",
	"Екатеринбург",
	"Нижний Новгород",
	"Казань",
	"Самара",
	"Омск",
	"Челябинск",
	"Ростов-на-Дону",
	"Волгоград",
	"Красноярск",
	"Пермь",
	"Уфа",
	"Челябинск"
];

function highlight(string, search) {
	var i = string.toLowerCase().indexOf(search.toLowerCase());
	search = string.substr(i, search.length);
	return string.replace(new RegExp(search, 'ig'), '<span class="search-mark">'+search+'</span>');
}

jQuery('.js__search-input').autocomplete({
	source: availableTags,
	create: function(event, ui) {
		// jQuery('#ui-id-1').scrollbar()
	},
	open: function(event, ui) {
		jQuery('#ui-id-1').css('top', parseFloat(jQuery('#ui-id-1').css('top')) + 2).find('li > div').each(function() {
			var text = jQuery(this).text();
			var search = jQuery('.js__search-input').val();
			jQuery(this).html(highlight(text, search.toLowerCase()))
		})
	},
	change: function(event, ui) {
		if (jQuery(this).val() !== '') {
			jQuery('.search-modal--submit').removeClass('disabled')
		} else {
			jQuery('.search-modal--submit').addClass('disabled')
		}
	}
});



jQuery('.js__header-city').click(function() {
	var modal = jQuery('.search-modal.js__modal');
	var width = jQuery(modal).outerWidth();
	var left = (jQuery(window).outerWidth() - width)/2;
	jQuery(modal).attr('data-left', left)
	jQuery('.opacity').fadeIn(300);
	setTimeout(function() {
		jQuery(modal).css({left: jQuery(window).outerWidth() + width}).show()
		jQuery(modal).animate({left: left}, 500);	
	}, 100)
})


