// Firefox and IE Fix Svg animation

// var ua = window.navigator.userAgent;
// var msie = ua.indexOf("MSIE ");
// if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
// 	jQuery('.view-button').addClass('using-ie')
// }

// jQuery('.view-button.view-button_active').find('rect').each(function() {
// 	var w = jQuery(this).attr('width')
// 	var h = jQuery(this).attr('height')
// 	jQuery(this).attr('data-width',w).attr('data-height', h)
// 	jQuery(this).attr('width', 9).attr('height', 4.5)
// })
// jQuery('.view-button').on('mouseover', function() {
// 	jQuery(this).find('rect').each(function() {
// 		var w = jQuery(this).attr('width')
// 		var h = jQuery(this).attr('height')
// 		jQuery(this).attr('data-width',w).attr('data-height', h)
// 		jQuery(this).attr('width', 9).attr('height', 4.5)
// 	})
// })
// jQuery('.view-button').on('mouseout', function() {
// 	jQuery(this).find('rect').each(function() {
// 		jQuery(this).attr('width', jQuery(this).attr('data-width')).attr('height', jQuery(this).attr('data-height'))
// 	})
// })
jQuery('.view-button').on('click', function() {
	var block = jQuery(this).parent()
	jQuery(block).find('.view-button_active').removeClass('view-button_active')
	jQuery(this).addClass('view-button_active')
})

// Select Boxes

jQuery('.select--active-block').on('click', function() {
	var select = jQuery(this).closest('.select');
	select.toggleClass('select_shown');
})

jQuery('.select--option').on('click', function() {
	var select = jQuery(this).closest('.select');
	var text = jQuery(this).text();
	select.find('option').each(function() {
		if (jQuery(this).text() === text) {
			select.find('select').val(jQuery(this).val());
			return false;
		}
	});
	select.find('.select--option_chosen').removeClass('select--option_chosen');
	jQuery(this).addClass('select--option_chosen');
	select.find('.select--active').text(text)
	select.toggleClass('select_shown');
})

$(document).mouseup(function (e) {
    var select = jQuery('.select');
    if (select.has(e.target).length === 0){
        select.removeClass('select_shown');
    }
});

// Favorite button

jQuery('.js__favorite-button').click(function() {
	var button = jQuery(this);
	if (button.hasClass('favorite')) {
		button.text('Добавить статью в избранное')
		button.append('<span><svg class="icon"><use xlink:href="./img/sprite.svg#icons--015-star"></use></svg></span>')
	} else {
		button.text('Статья уже в избранном')
		button.append('<span><svg class="icon"><use xlink:href="./img/sprite.svg#icons--015-star-full"></use></svg></span>')
	}
	button.toggleClass('favorite')
})
jQuery('.js__partner-button').click(function() {
	var button = jQuery(this);
	if (button.hasClass('favorite')) {
		button.text('Стать партнером')
		button.append('<span><svg class="icon"><use xlink:href="./img/sprite.svg#icons--015-star"></use></svg></span>')
	} else {
		button.text('Вы уже партнер')
		button.append('<span><svg class="icon"><use xlink:href="./img/sprite.svg#icons--015-star-full"></use></svg></span>')
	}
	button.toggleClass('favorite')
})

// Toggle
jQuery('.button-small.js__toggle').click(function(){
	jQuery(this).toggleClass('button-small_active')
})